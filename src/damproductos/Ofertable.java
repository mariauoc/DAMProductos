/*
 * Interface para los productos que tienen dto
 */
package damproductos;

/**
 *
 * @author stucom
 */
public interface Ofertable {
    public double calcularPvpConDescuento();
}
