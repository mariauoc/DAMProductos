/*
 * Electronica hereda de Producto, pero sigue siendo abstracta
 */
package damproductos;

/**
 *
 * @author stucom
 */
public abstract class Electronica extends Producto {
    
    private String fabricante;

    public Electronica(String fabricante, String descripcion, double coste) {
        super(descripcion, coste);
        this.fabricante = fabricante;
    }
    
    

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

}
