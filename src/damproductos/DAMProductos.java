/*
 * Ejemplo de herencia con clases abstractas e interfaces
 */
package damproductos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mar
 */
public class DAMProductos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<Producto> misProductos = new ArrayList<>();
        Libro miLibro = new Libro("Alfaguara", "La historia interminable", 18);
        misProductos.add(miLibro);
        Television tv = new Television(40, "Sony", "Sony SmartTV LaPera", 400);
        misProductos.add(tv);
        
        for (Producto p : misProductos) {
            System.out.println(p.getDescripcion()+" pvp: "+p.calcularPVP()+"€");
            if (p instanceof Ofertable) {
                Ofertable oferta = (Ofertable) p;
                System.out.println("Precio con descuento: "+oferta.calcularPvpConDescuento()+" €");
            }
        }
        
        
    }
    
}
