/*
 * Libro hereda de Producto
 */
package damproductos;

/**
 *
 * @author stucom
 */
public class Libro extends Producto implements Ofertable {

    private String editorial;

    public Libro(String editorial, String descripcion, double coste) {
        super(descripcion, coste);
        this.editorial = editorial;
    }
    
    

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    @Override
    public double calcularPVP() {
        return getCoste()+getCoste()*0.25;
    }

    @Override
    public double calcularPvpConDescuento() {
        return calcularPVP()-calcularPVP()*0.05;
    }

}
