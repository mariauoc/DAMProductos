/*
 * Television hereda de Electronica
 */
package damproductos;

/**
 *
 * @author stucom
 */
public class Television extends Electronica {

    private int pulgadas;

    public Television(int pulgadas, String fabricante, String descripcion, double coste) {
        super(fabricante, descripcion, coste);
        this.pulgadas = pulgadas;
    }

    public int getPulgadas() {
        return pulgadas;
    }

    public void setPulgadas(int pulgadas) {
        this.pulgadas = pulgadas;
    }

    @Override
    public double calcularPVP() {
        return getCoste()+getCoste()*0.20+pulgadas;
    }

}
