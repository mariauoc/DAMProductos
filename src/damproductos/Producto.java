/*
 * Clase abstracta Producto
 */
package damproductos;

/**
 *
 * @author mar
 */
public abstract class Producto {
    
    private String descripcion;
    private double coste;

    public Producto(String descripcion, double coste) {
        this.descripcion = descripcion;
        this.coste = coste;
    }
    
    public abstract double calcularPVP();

    public double getCoste() {
        return coste;
    }

    public void setCoste(double coste) {
        this.coste = coste;
    }


    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
